`linux` module API docs
====================================================

.. automodule:: pumpkin_supmcu.linux

`I2CLinuxMaster` module API docs
--------------------------------

.. autoclass:: pumpkin_supmcu.linux.I2CLinuxMaster
    :members: