# pumpkin-supmcu-smbus Package

The `pumpkin-supmcu-smbus` package has the following functionality:

* Leverages business logic found in [pumpkin-supmcu](https://pumpkin-supmcu.readthedocs.io/en/latest/index.html).
    + Please read `pumpkin-supmcu` documentation for usage, this is the implementation package for [smbus2](https://pypi.org/project/smbus2/)
* Interfaces `pumpkin-supmcu` package with the [smbus2 package](https://pypi.org/project/smbus2/)

The documentation for the `pumpkin-supmcu-smbus` package can be [found here](https://pumpkin-supmcu-smbus.readthedocs.io/en/latest/).
